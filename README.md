# ECOS Unified
## Emergency Check Out System

ECOS is a mostly ILS neutral emergency check out system to use during power and network outages or at any time when the ILS cannot be used normally. Because the system relies on a method of scanning a patron's card, then scanning their items, it will work for almost any ILS that follows a similar workflow.

Built in Python and tkinter, ECOS should work well on Windows, macOS, and Linux. Additionally, I build a Windows exe file using pyinstaller. Backend data management is handled by a SQLite database.

### Note
ECOS was formerly known as PECOS, the Polaris Emergency Check Out System. You'll see references to that name throughout the code and files until I get that all cleaned up.
